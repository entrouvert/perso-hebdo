$(function() {
	//Affichage du calendrier en français à la one again
	$( "#calendrier").datepicker({
		firstDay: 1,
		altField: "#datepicker",
		closeText: 'Fermer',
		prevText: 'Précédent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
		weekHeader: 'Sem.',
		dateFormat: 'dd/mm/yy'
	});

	//Rends les tâches draggable
	$('ul').sortable({
		cursor: "move",
		opacity: 0.35,
		update: function(mouseup, item){
			fonctionListeTaches();
		},
	});
	$( '#gauche>button').click(function() {fonctionAjoutTache();});
	$( '.moins' ).click(function() {fonctionEffacer($(this).parent());}); // voir le machin live
	$('.nbre').change(function(){fonctionControle($(this));});
	$('#generer>button').click(function() {fonctionGantt();});
});

//Fonction pour faire la liste  les tâches
function fonctionListeTaches(){
	$('li').each(function(i){
		j = parseInt(i)+1;
		$(this).attr('id',j);
	})
	$('.moins').unbind('click');
	$('.nbre').unbind('change');
	$( '.moins' ).click(function() {fonctionEffacer($(this).parent());});
	$('.nbre').change(function(){fonctionControle($(this));});
}	

// Ajouter un tâche
function fonctionAjoutTache() {
	nouvelleTache =$('li:first').clone();
	$("ul").append(nouvelleTache);
	fonctionListeTaches();
}

//Supprimer une tâche
function fonctionEffacer(li){
	if ($('ul').children().length > 1){
		li.remove();
		fonctionListeTaches();
	}else{alert("Un projet doit contenir au moins une tâche, suppression impossible.");}
}

//Vérifier qu'on saisit bien un nombre dans le champs nombre de semaines
function fonctionControle(sem){
     var input = sem.val();
     var regex = new RegExp("^[0-9]+$");
     if (!regex.test(input)) {
	sem.css('background-color','red');
	alert("Il faut saisir ici un nombre de semaine(s) : uniquement des chiffres !");
	sem.val('1');
	sem.css('background-color','white');
     }
}

// Initialisation, puis appel des fonctions nécessaires pour faire le graphique
function fonctionGantt() {
	$('#mois').html('');
	$('#lignes').html('');
	nbreSemaines = 0;
	if ($('#calendrier').val() == ""){
		alert('Sélectionnez une date de début de projet');
		return;
	}
	fonctionDates();
	fonctionDessin();
}

// Calculs sur les dates : il faut calculer la largeur du graph en nombre de jours et pas en mois.
function fonctionDates(){
	dateDebut = new Date($('#calendrier').datepicker('getDate'));
	dateFin = new Date($('#calendrier').datepicker('getDate'));
	$('.nbre').each(function(i){
		nbreSemaines = nbreSemaines + parseInt($(this).val());
	})
	dateFin.setDate(dateFin.getDate()+nbreSemaines*7);
	dureeEnJours = (dateFin.getTime() - dateDebut.getTime())/86400000;
	if (dureeEnJours  > 365){
		alert("Si votre projet dure plus d'un an c'est qu'il est mal conçu, il faut le découper en plusieurs projets");
		return;
	}
	moisDebut = dateDebut.getMonth();
	moisFin = dateFin.getMonth();
	if (dateFin.getFullYear() > dateDebut.getFullYear()){moisFin = moisFin+12;}
	nbreMois = moisFin-moisDebut;
}

//Dessin du gantt
function fonctionDessin() {
	mois = [["jan",31],["fév",28],["mar",31],["avr",30],["mai",31],["juin",30],["juil",31],["aoû",31],["sep",30],["oct",31],["nov",30],["déc",31]];
	//Pour faire varier la taille du graph en fonction de la durée du projet
	echelle = Math.round(365/dureeEnJours)*1,5;
	if (echelle > 6){echelle = 6;}
	//Libellés des mois (je devrais faire une boucle en utilisant les dates plutôt, je sais pas faire...)
	for (var i=moisDebut;  i<=moisFin; i++){ 
		if ((dateDebut.getYear()%4 ==0 && i<13) || (dateFin.getYear()%4 ==0 && i>13)){mois[1][1] == 29;}//pour les années bisextiles
		$('#mois').append('<div>'+mois[i%12][0]+'</div>');
		largeurMois = mois[i%12][1]/10*echelle;
		$('#mois>div:last').css('width',largeurMois+'em')
	}
	debutGraph = dateDebut.getDate()/10*echelle;
	var j =1;
	nbreJoursTache=0;
	//Boucle pour afficher les libellés et les graphiques
	while ($('#'+j).length){
		libelle = $('#'+j+'>input:nth-child(2)').val();
		nbreJoursTache = parseInt($('#'+j+'>input:nth-child(3)').val())*7;
		longueurGraph = nbreJoursTache/10*echelle;
		//Écriture du libellé et du graph sur une seule ligne
		ligne = '<p><div id="libelles">'+libelle+'</div><div id="barre" style="margin-left: '+debutGraph+'em; width: '+longueurGraph+'em;">&nbsp;</div></p>'
		$('#lignes').append(ligne);
		if (j%2==0){$('#lignes>p:last').css('background-color','grey');}
		debutGraph = debutGraph + longueurGraph;
		j++;
	}
}
